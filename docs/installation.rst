============
Installation
============

At the command line either via easy_install or pip::

    $ easy_install configkeeper
    $ pip install configkeeper

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv configkeeper
    $ pip install configkeeper
